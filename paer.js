(() => {

    const GAME_SPACE = document.getElementById('game-space');
    const LINE_INPUT = document.getElementById('line-input');
    const COLUMN_INPUT = document.getElementById('column-input');
    const START_BTN = document.getElementById('start-btn');
    LINE_INPUT.placeholder = 'Введите число строк: 4';
    COLUMN_INPUT.placeholder = 'Введите число колонок: 4';

    function arrayNamberGeneration(line, column) {
        let LENGTH = null;
        let newLine = null;
        let newColumn = null;
        if(line%2 && column%2) {
            newLine = 4;
            newColumn = 4;
        }
        else {
            newLine = line;
            newColumn = column;
        }
        LENGTH = newLine * newColumn / 2;
        let arrayOfNumber = [];
        for(let i = 0; i < LENGTH; ++i) {
            arrayOfNumber.push(i+1);
            arrayOfNumber.push(i+1);
        };
        return {
            arrayOfNumber,
            newLine,
            newColumn,
        };
    };

    function checkEmptyAndGeneration(line, column) {
        intLine = Number(line);
        intColumn = Number(column);
        console.log(Boolean(intColumn%2));
        if(isNaN(intLine) && intColumn) {
            if(intColumn < 2 || intColumn > 10) {
                return arrayNamberGeneration(4, 4);
            };
            return arrayNamberGeneration(4, intColumn);
        }
        else if(intLine && isNaN(intColumn)) {
            if(intLine < 2 || intLine > 10) {
                return arrayNamberGeneration(4 , 4);
            };
            return arrayNamberGeneration(intLine, 4);
        }
        else if(isNaN(intLine) && isNaN(intColumn)) {
            return arrayNamberGeneration(4, 4);
        }
        else {
            if((intLine < 2 || intLine > 10) && (intColumn < 2 || intColumn > 10)) {
                return arrayNamberGeneration(4, 4);
            }
            else if((intLine < 2 || intLine > 10) && (intColumn > 2 || intColumn < 10)) {
                return arrayNamberGeneration(4, intColumn);
            }
            else if((intLine > 2 || intLine < 10) && (intColumn < 2 || intColumn > 10)) {
                return arrayNamberGeneration(intLine, 4);
            }
            return arrayNamberGeneration(intLine, intColumn);
        }
    };

    function randomOrder(array = []) {
        for(let i = array.length - 1; i > 0; --i) {
            let ind = array[i];
            let rnd = Math.floor(Math.random() * (i + 1));
            array[i] = array[rnd];
            array[rnd] = ind;
        }
        return array;
        // let newArray = [];
        // while (array.length > 0) {
        //     let range = array.length;
        //     let randomIdex = Math.floor(Math.random() * range);
        //     newArray.push(array[randomIdex]);
        //     array.splice(randomIdex, 1);
        // }
        // return newArray;
    };

    function cardsGeneration(gameSpace, array =[], lineValue, columnValue, ckeckArray, eventCkick) {
        for (let i = 0; i < array.length; ++i) {
            const CARD = document.createElement('div');
            const CARD_VALUE = document.createElement('span');
            CARD_VALUE.classList.add('number');
            CARD_VALUE.style.display = 'none';
            CARD_VALUE.textContent = array[i];
            if(lineValue%2 && columnValue%2) {
                CARD.classList.add('cc-4')
            }
            else {
                switch(columnValue) {
                    case 2:
                        CARD.classList.add('cc-2')
                        break
                    case 3:
                        CARD.classList.add('cc-3')
                        break
                    case 5:
                        CARD.classList.add('cc-5')
                        break
                    case 6:
                        CARD.classList.add('cc-6')
                        break
                    case 7:
                        CARD.classList.add('cc-7')
                        break
                    case 8:
                        CARD.classList.add('cc-8')
                        break
                    case 9:
                        CARD.classList.add('cc-9')
                        break
                    case 10:
                        CARD.classList.add('cc-10')
                        break
                }
            }

            CARD.addEventListener('click', (event) => {
                function displayNone(ckeckArray) {
                    ckeckArray.firstChild.style.display = 'none'
                }
                let value = event.target;
                CARD_VALUE.style.display = '';
                eventCkick++;
                if(eventCkick === 3 && ckeckArray.length === 2) {
                    eventCkick = 1
                    let one = parseInt(ckeckArray[0].firstChild.textContent);
                    let two = parseInt(ckeckArray[1].firstChild.textContent);
                    if (one !== two) {
                        displayNone(ckeckArray[0]);
                        displayNone(ckeckArray[1]);
                    }
                    ckeckArray = [];
                }
                ckeckArray.push(value);
            })
            CARD.classList.add("card");
            CARD.append(CARD_VALUE);
            gameSpace.append(CARD);
        };
    };

    function clearGameSpace(gameSpace) {
        while (gameSpace.firstChild) {
            gameSpace.removeChild(gameSpace.firstChild);
        }
    }

    let creatGame = function(event) {
        let ckeckArray = [];
        let eventCkick = 0;
        event.preventDefault();
        const LINE_VALUE = parseInt(LINE_INPUT.value);
        const COLUMN_VALUE = parseInt(COLUMN_INPUT.value);
        const ARRAY_OF_NUMBER = checkEmptyAndGeneration(LINE_VALUE, COLUMN_VALUE);
        const ARRAY_OF_RANDOM_NUMBER = randomOrder(ARRAY_OF_NUMBER.arrayOfNumber);
        clearGameSpace(GAME_SPACE);
        cardsGeneration(GAME_SPACE, ARRAY_OF_RANDOM_NUMBER, ARRAY_OF_NUMBER.newLine, ARRAY_OF_NUMBER.newColumn, ckeckArray, eventCkick);
        START_BTN.textContent = 'Начать заново';
    }
    
    document.addEventListener('DOMContentLoaded', () => {  
        START_BTN.addEventListener('click', creatGame);
        
    })

})();